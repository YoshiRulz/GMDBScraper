package net.gratismusicdb.yt_scraper

import kotlinx.serialization.Serializable

internal object InternalAPIA {
	@Serializable
	class YTInitialData(
		val contents: Contents? = null,
		val continuationContents: ContinuationContents? = null,
		val onResponseReceivedActions: List<OnResponseReceivedActions>? = null,
	) {
		@Serializable
		class Contents(
			val singleColumnBrowseResultsRenderer: SingleColumnBrowseResultsRenderer? = null,
			val twoColumnBrowseResultsRenderer: TwoColumnBrowseResultsRenderer? = null,
			val twoColumnSearchResultsRenderer: TwoColumnSearchResultsRenderer? = null,
			val twoColumnWatchNextResults: TwoColumnWatchNextResults? = null,
		)

		@Serializable
		class ContinuationContents(
			val playlistVideoListContinuation: PlaylistVideoListRenderer,
		)

		@Serializable
		class OnResponseReceivedActions(
			val appendContinuationItemsAction: AppendContinuationItemsAction,
		) {
			@Serializable
			class AppendContinuationItemsAction(
				val continuationItems: List<PlaylistVideoListRenderer.Contents> = emptyList(),
			)
		}
	}

	@Serializable
	class TwoColumnWatchNextResults(
		val results: Results,
	) {
		@Serializable
		class Results(
			val results: ResultsResults,
		) {
			@Serializable
			class ResultsResults(
				val contents: List<Contents> = emptyList(),
			)
		}

		@Serializable
		class Contents(
			val videoSecondaryInfoRenderer: VideoSecondaryInfoRenderer? = null,
		)

		@Serializable
		class VideoSecondaryInfoRenderer(
			val description: Text? = null,
		)
	}

	interface HasTabs {
		val tabs: List<Tab>?

		@Serializable
		class Tab(
			val tabRenderer: TabRenderer,
		)
	}

	@Serializable
	class SingleColumnBrowseResultsRenderer(
		override val tabs: List<HasTabs.Tab> = emptyList(),
	): HasTabs

	@Serializable
	class TwoColumnBrowseResultsRenderer(
		override val tabs: List<HasTabs.Tab> = emptyList(),
	): HasTabs

	@Serializable
	class TwoColumnSearchResultsRenderer(
		val primaryContents: PrimaryContents? = null,
	) {
		@Serializable
		class PrimaryContents(
			val sectionListRenderer: SectionListRenderer? = null,
		)
	}

	@Serializable
	class TabRenderer(
		val selected: Boolean? = null,
		val content: Content? = null,
	) {
		@Serializable
		class Content(
			val sectionListRenderer: SectionListRenderer? = null,
		)
	}

	@Serializable
	class SectionListRenderer(
		val contents: List<Contents> = emptyList(),
	) {
		@Serializable
		class Contents(
			val itemSectionRenderer: ItemSectionRenderer? = null,
			val playlistVideoListRenderer: PlaylistVideoListRenderer? = null,
		)
	}

	@Serializable
	class ItemSectionRenderer(
		val contents: List<Contents> = emptyList(),
	) {
		@Serializable
		class Contents(
			val channelAboutFullMetadataRenderer: ChannelAboutFullMetadataRenderer? = null,
			val channelRenderer: ChannelRenderer? = null,
			val playlistVideoListRenderer: PlaylistVideoListRenderer? = null,
		)
	}

	@Serializable
	class ChannelAboutFullMetadataRenderer(
		val primaryLinks: List<Link> = emptyList(),
	) {
		@Serializable
		class Link(
			val title: Text,
		)
	}

	@Serializable
	class PlaylistVideoListRenderer(
		val contents: List<Contents> = emptyList(),
		val continuations: List<Continuation> = emptyList(),
	) {
		@Serializable
		class Contents(
			val continuationItemRenderer: ContinuationItemRenderer? = null,
			val playlistVideoRenderer: PlaylistVideoRenderer? = null,
		)

		@Serializable
		class Continuation(
			val nextContinuationData: ContinuationData,
		) {
			@Serializable
			class ContinuationData(
				val continuation: String,
			)
		}
	}

	@Serializable
	class ContinuationItemRenderer(
		val continuationEndpoint: ContinuationEndpoint,
	) {
		@Serializable
		class ContinuationEndpoint(
			val continuationCommand: ContinuationCommand,
		) {
			@Serializable
			class ContinuationCommand(
				val token: String,
			)
		}
	}

	@Serializable
	class PlaylistVideoRenderer(
		val videoId: YTVideoID,
		val title: Text,
		val shortBylineText: Text,
		val lengthSeconds: String,
		val isPlayable: Boolean? = null, // always... true? with mimicAndroid at least
	)

	@Serializable
	class ChannelRenderer(
		val channelId: String,
		val title: Text,
		val videoCountText: Text? = null,
		val ownerBadges: List<OwnerBadge> = emptyList(),
		val subscriberCountText: Text? = null,
		val subscribeButton: SubscribeButton? = null,
	) {
		@Serializable
		class SubscribeButton
	}

	@Serializable
	class Text(
		val simpleText: String? = null,
		val runs: List<RunSegment> = emptyList(),
	) {
		@Serializable
		class RunSegment(
			val text: String,
			val navigationEndpoint: NavigationEndpoint? = null,
		)
	}

	@Serializable
	class NavigationEndpoint(
		val browseEndpoint: BrowseEndpoint? = null,
	) {
		@Serializable
		class BrowseEndpoint(
			val browseId: String,
		)
	}

	@Serializable
	class OwnerBadge(
		val metadataBadgeRenderer: Badge? = null,
	) {
		@Serializable
		class Badge(
			val tooltip: String,
		)
	}
}
