package net.gratismusicdb.yt_scraper

import kotlinx.serialization.Serializable

/**
 * using this to enumerate playlists because the "browser mimic" approach for that looked to be annoying
 *
 * get video example: `{"context": {"client": {"hl": "en", "clientName": "WEB", "clientVersion": "2.20210721.00.00", "clientFormFactor": "UNKNOWN_FORM_FACTOR", "clientScreen": "WATCH", "mainAppWebInfo": {"graftUrl": "/watch?v=UF8uR6Z6KLc"}}, "user": {"lockedSafetyMode": false}, "request": {"useSsl": true, "internalExperimentFlags": [], "consistencyTokenJars": []}}, "videoId": "UF8uR6Z6KLc", "playbackContext": {"contentPlaybackContext": {"vis": 0, "splay": false, "autoCaptionsDefaultOn": false, "autonavState": "STATE_NONE", "html5Preference": "HTML5_PREF_WANTS", "lactMilliseconds": "-1"}}, "racyCheckOk": false, "contentCheckOk": false}` (to `https://www.youtube.com/youtubei/v1/player?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8`)
 */
internal object APIv1 {
	@Serializable
	@Suppress("unused")
	class BrowseRequestParameters(
		val browseId: String,
		val continuation: String?,
		val context: Context = Context(),
	) {
		@Serializable
		class Context(
			val client: ClientContext = mimicAndroid, // using `mimicWeb`, presumably matching web app, would cause playlist enumeration to miss some region-locked videos
			val user: UserContext = UserContext(),
		) {
			@Serializable
			class ClientContext(
				val clientName: String,
				val clientVersion: String,
				val newVisitorCookie: Boolean = true,
				val hl: String = "en",
				val gl: String = "US",
				val utcOffsetMinutes: Int = 0,
			)

			@Serializable
			class UserContext(
				val lockedSafetyMode: Boolean = false,
			)

			companion object {
				private val mimicAndroid = ClientContext(clientName = "ANDROID", clientVersion = "16.20")

				private val mimicWeb = ClientContext(clientName = "WEB", clientVersion = "2.20210408.08.00")
			}
		}
	}

	data class ContinuationToken(val raw: String)

	/** run `ytcfg.data_.INNERTUBE_API_KEY` in console on any(?) YouTube web app page */
	private const val API_KEY = "AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8"

	suspend fun browse(
		playlistID: YTPlaylistID,
		httpPOST: suspend (uri: String, bodyJSON: BrowseRequestParameters) -> InternalAPIA.YTInitialData,
		internContToken: ContinuationToken? = null,
	): InternalAPIA.YTInitialData
		= httpPOST(
			"https://www.youtube.com/youtubei/v1/browse?key=$API_KEY",
			BrowseRequestParameters(browseId = "VL$playlistID", continuation = internContToken?.raw)
		)
}
