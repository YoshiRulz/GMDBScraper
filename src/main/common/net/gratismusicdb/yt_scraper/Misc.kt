@file:Suppress("NOTHING_TO_INLINE")

package net.gratismusicdb.yt_scraper

import dev.yoshirulz.lib.strings.HTMLString
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.utils.io.core.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.serialization.json.Json

/** not really rate *limiting* so much as artificially delaying */
public class RateLimitedHTTPClient(private val wrapped: HttpClient): Closeable by wrapped {
	public constructor(block: HttpClientConfig<*>.() -> Unit = goodIdeaHTTPConfig): this(HttpClient(block))

	public suspend fun get(uri: String): HTMLString
		= coroutineScope {
			delay(100)
			try {
				return@coroutineScope HTMLString(wrapped.get(uri).body<String>())
			} catch (t: Throwable) {
				// ignore
			}
			delay(500)
			try {
				return@coroutineScope HTMLString(wrapped.get(uri).body<String>())
			} catch (t: Throwable) {
				// ignore
			}
			delay(1000)
			return@coroutineScope HTMLString(wrapped.get(uri).body<String>())
		}

	public suspend inline fun <reified T> postJSON(uri: String, bodyObj: Any): T
		= postJSONRaw(uri, bodyObj).body()

	public suspend fun postJSONRaw(uri: String, bodyObj: Any): HttpResponse
		= coroutineScope {
			delay(100)
			wrapped.post(uri) {
				contentType(ContentType.Application.Json)
				setBody(bodyObj)
			}
		}

	public companion object {
		private val goodIdeaHTTPConfig: HttpClientConfig<*>.() -> Unit = {
			install(ContentNegotiation) {
				json(JSON)
			}
			install(UserAgent) {
				agent = "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0" // fuck you (this is Tor Browser)
			}
		}
	}
}

public val JSON: Json = Json {
	encodeDefaults = true
	ignoreUnknownKeys = true
}
