package net.gratismusicdb.yt_scraper

import dev.yoshirulz.lib.strings.*

internal expect fun HTMLString.getInitialDataJSON(): JSONString

internal suspend fun RateLimitedHTTPClient.getAboutPageFor(id: YTChannelID): HTMLString
	= this.get("https://www.youtube.com/channel/$id/about")

internal suspend fun RateLimitedHTTPClient.getChannelSearchPage(act: String): HTMLString
	= this.get("https://www.youtube.com/results?search_query=${act.lowercase().replace(' ', '+')}&sp=EgIQAg%3D%3D") // this is intentionally double-encoded (triple if you include base64)

internal suspend fun RateLimitedHTTPClient.getVideoWatchPageFor(id: YTVideoID): HTMLString
	= this.get("https://www.youtube.com/watch?v=$id")

internal suspend fun RateLimitedHTTPClient.getChannelAboutPageInitialData(id: YTChannelID): JSONString
	= this.getAboutPageFor(id).getInitialDataJSON()

internal suspend fun RateLimitedHTTPClient.getVideoDescription(id: YTVideoID): List<String>
	= NiceAPI.parseVideoDescription(this.getVideoWatchPageFor(id).getInitialDataJSON())
