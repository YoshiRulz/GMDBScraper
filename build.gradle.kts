@file:Suppress("SpellCheckingInspection", "UNUSED_VARIABLE")

import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
	application
	kotlin("multiplatform") version "1.7.0"
	kotlin("plugin.serialization") version "1.7.0"
	id("com.github.ben-manes.versions") version "0.42.0"
}

repositories {
	mavenCentral()
	mavenLocal()
}

kotlin {
	jvm { // :run
		val javaTargetVersion = JavaLanguageVersion.of(17)
		jvmToolchain {
			languageVersion.set(javaTargetVersion)
		}
		withJava()
		compilations.all {
			kotlinOptions.jvmTarget = javaTargetVersion.toString()
		}
		project.application {
			mainClass.set("net.gratismusicdb.yt_scraper.MainKt")
		}
	}

//	linuxX64 { // :runReleaseExecutableLinux
//		binaries {
//			executable {
//				entryPoint = "net.gratismusicdb.yt_scraper.main"
//			}
//		}
//	}

	explicitApi()

	sourceSets {
		fun creatingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {})
			= creating {
				parent?.let(::dependsOn)
				configuration(this)
			}

		fun gettingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {})
			= getting {
				parent?.let(::dependsOn)
				configuration(this)
			}

		fun depStr(partialCoords: String, version: String)
			= "$partialCoords:$version"

		fun kotlinx(module: String, version: String)
			= depStr("org.jetbrains.kotlinx:kotlinx-$module", version)

		fun coroutines(module: String)
			= kotlinx("coroutines-$module", "1.6.3")

		fun serialization(module: String)
			= kotlinx("serialization-$module", "1.3.3")

		fun ktor(module: String)
			= depStr("io.ktor:ktor-$module", "2.0.3")

		val commonMain by gettingWithParent(null) {
			dependencies {
				implementation(coroutines("core"))
				implementation(serialization("core"))
				implementation(serialization("json"))
				implementation(serialization("protobuf"))
				implementation(ktor("client-content-negotiation"))
				implementation(ktor("client-core"))
				implementation(ktor("client-json"))
				implementation(ktor("client-serialization"))
				implementation(ktor("serialization-kotlinx-json"))
				implementation(depStr("dev.yoshirulz:yosher-lib", "0.1.0"))
				implementation(depStr("dev.yoshirulz:ksoup321", "1.14.2-SNAPSHOT"))
			}
		}

		val jvmMain by gettingWithParent(commonMain) {
			dependencies {
				implementation(ktor("client-cio"))
			}
		}

//		val linuxX64Main by creatingWithParent(commonMain) {
//			dependencies {
//				implementation(ktor("client-curl"))
//			}
//		}

		all {
			kotlin.srcDir("src/${name.takeLast(4).toLowerCase()}/${name.dropLast(4)}")
		}
	}
}

tasks.withType<com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask> {
	rejectVersionIf {
		candidate.version.toLowerCase().let {
			it.contains("-rc") || it.contains("-m") || it.contains("-beta") || it.contains("-alpha")
		}
	}
}
